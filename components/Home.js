import React from 'react';
import { StyleSheet, ActivityIndicator, FlatList, Text, ScrollView, SafeAreaView, View, Dimensions, Image, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLoading: true, dataSource: [] };
    }

    componentDidMount() {     
        this.willBlurListener = this.props.navigation.addListener('focus', () => {
            this.getData()
        })
    }

    getData = async () => {
        await AsyncStorage.getItem('@store', (error, result) => {
            if (error !== null) { 
                this.setState(
                    {
                        isLoading: false
                    },
                    function () {   }
                );
            } else {
                this.setState(
                    { 
                        isLoading: false,
                        dataSource: JSON.parse(result)
                    }, 
                    function () {   }
                );
            }
        });
    }

    _onPress = (item) => {
        this.props.navigation.navigate('Produit', { code : item.code, new: false, date: item.scanDate } ) 
    };

    nutriScore = (item) => {
        let color;
        switch (item.product.nutriscore_grade) {
            case "a":
                color = "green"
                break
            case "b":
                color = "green"
                break
            case "c":
                color = "yellow"
                break
            case "d":
                color = "orange"
                break
            case "e":
                color = "red"
                break
            default: 
                break
        }
        return <View style={{backgroundColor:color, width:24, height:24, alignContent:"center", borderRadius:12, alignItems:"center", marginHorizontal:8}}><Text style={{color:"white", marginTop:3}}>{item.product.nutriscore_grade.toUpperCase()}</Text></View>
    };

    _goToScan = () => {
        this.props.navigation.navigate('Scanner', { screen: 'Scanner' });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                    <Text style={{ flex:1, marginTop: 32, marginLeft: 16, fontSize:22}}>Accueil</Text>
                    <View style={{ flex: 1, paddingVertical: Dimensions.get('window').height / 2.5}}>
                        <ActivityIndicator />
                    </View>
                </SafeAreaView>
            );
        }

        if (this.state.dataSource == null) {
            return (
                <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                    <Text style={{ marginTop: 32, marginLeft: 16, fontSize:22}}>Accueil</Text>
                    <View style={{ flex: 1, justifyContent:"center", alignItems: "center", verticalAlign: "middle",}}>
                        <Text style={{fontSize:24, fontWeight: "bold"}}>Aucun article scanné</Text>
                    </View>
                </SafeAreaView>
            );
        }

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                <View style={{flexDirection: "row", justifyContent: "space-between", marginHorizontal: 16, marginBottom:16, }}>
                    <Text style={{fontSize:22, marginTop: 32}}>Accueil</Text>
                    <TouchableOpacity style={{alignSelf:"flex-end", height:30, width:30, borderRadius: 15, borderWidth:1, marginTop: 32}} onPress={this._goToScan}>
                        <Text style={{ fontSize: 20, color: 'black', flex: 1, alignSelf:"center" }}>+</Text>
                    </TouchableOpacity>
                </View>
                <FlatList 
                    data={this.state.dataSource != null ? this.state.dataSource.filter((e) => e.product !== undefined) : null } 
                    keyExtractor={(item) => item.code + Math.random().toString(36).substring(7) }
                    renderItem={({ item }) => 
                    <TouchableOpacity onPress={() => this._onPress(item)}>
                        <View style={styles.item}>
                            <Image style={styles.image}
                                source={{
                                    uri: item.product.selected_images.front.display.fr
                                }}
                            />
                            <View style={{flex : 1, flexDirection: "column", marginTop: 8}}>
                                <Text style={{marginHorizontal: 8, fontSize: 18, fontWeight: "bold"}}>{item.product.product_name}</Text> 
                                <Text style={{marginHorizontal: 8, fontSize: 16, color: "gray"}}>{item.product.brands_tags[0].charAt(0).toUpperCase() + item.product.brands_tags[0].slice(1)}</Text>
                                <View style={{marginTop:0, flex: 1, flexDirection: "row", justifyContent: "space-between"}}>
                                    {item.scanDate != null
                                        ? <Text style={styles.date}>Scanné le { item.scanDate }</Text>
                                        : null
                                    }
                                    {item.product.nutriscore_grade != null 
                                        ? this.nutriScore(item)
                                        : null
                                    }                                
                                </View>
                            </View> 
                        </View>
                    </TouchableOpacity>
                    } 
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
    },
    image: {
        width: 100,
        height: 100,
    },
    item: {
        flex:1,
        flexDirection: "row",
        borderColor: 'black',
        borderBottomWidth: 1,
        fontSize: 18,
        height: 100,
    },
    date: {
        marginTop:4,
        marginHorizontal: 8,
        fontSize: 14,
        color: "gray",
        opacity: 0.5
    },
});
