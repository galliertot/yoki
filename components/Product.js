import React from 'react';
import { StyleSheet, ActivityIndicator, Text, SafeAreaView, View, Dimensions, Image, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TouchableOpacity } from 'react-native';


export default class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = { product: null, isLoading: true, code: props.route.params.code, date: props.route.params.date, new: props.route.params.new };
    }

    componentDidMount() {
        this.getData()
    }

    getData = async () => {
        if (this.state.new) {
            fetch('https://world.openfoodfacts.org/api/v0/product/' + this.state.code + '.json')
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.status == 1) {
                    this.setState(
                        {
                            isLoading: false,
                            product: responseJson
                        },
                        function() {}
                    );
                    this.addToStorage()
                } else {
                    this.state.isLoading = false
                    throw new Error("Code bar invalide")
                }
            })
            .catch(error => {
                console.error(error);
            });
        } else {
            try {
                const value = await AsyncStorage.getItem('@store');
                let array = JSON.parse(value)
                this.setState(
                    {
                        isLoading: false,
                        product: array.find(element => (element.code === this.state.code && element.scanDate === this.state.date))
                    },
                    function() {}
                );
            } catch (error) { throw new Error(error) }
        }        
    }

    addToStorage = async () => {
        if (this.state.new) {
            try {
                const value = await AsyncStorage.getItem('@store');
                if (value !== null) {
                    let array = JSON.parse(value)
                    this.state.product.scanDate = new Date().toLocaleDateString()
                    array.push(this.state.product)
                    try {
                        await AsyncStorage.setItem('@store', JSON.stringify(array));
                    } catch (error) { 
                        console.log(error) 
                    } 
                } else {
                    let array = Array();
                    this.state.product.scanDate = new Date().toLocaleDateString()
                    array.push(this.state.product)
                    try {
                        await AsyncStorage.setItem('@store', JSON.stringify(array));
                    } catch (error) { 
                        console.log(error) 
                    } 
                }          
            } catch (error) { console.log(error) }
        }
    }

    nutriScore = (item) => {
        let color;
        switch (item.product.nutriscore_grade) {
            case "a":
                color = "green"
                break
            case "b":
                color = "green"
                break
            case "c":
                color = "yellow"
                break
            case "d":
                color = "orange"
                break
            case "e":
                color = "red"
                break
            default: 
                break
        }
        return <View style={{backgroundColor:color, borderRadius:50, alignContent:"center", alignItems:"center", flex:1}}><Text style={{color:"white", marginTop:14, fontSize:18}}>{item.product.nutriscore_grade.toUpperCase()}</Text></View>
    }

    _info = (name, data, type) => {
        return (
            <View style={{ flex:1, flexDirection: 'row', justifyContent: "space-between", borderBottomWidth: 1, borderColor:"black", marginTop:16, paddingBottom:8}}>
                <Text style={styles.titleInfo}>{name}</Text>
                <Text style={styles.info}>{data} {type}</Text>
            </View>
        )
    }

    _goToScan = () => {
        this.props.navigation.navigate('Scanner', { screen: 'Scanner' });
    }

    render() {
        if (this.state.isLoading || this.state.product == null) {
            return (
                <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                    <View style={{ flex: 1, paddingVertical: Dimensions.get('window').height / 2.5}}>
                        <ActivityIndicator />
                    </View>
                </SafeAreaView>
            );
        }

        if (!this.state.isLoading) {
            return (
                <SafeAreaView>
                    <ScrollView  contentContainerStyle={{paddingBottom: 80}}>    
                        <Image style={styles.imageHeader}
                            source={{
                                uri: this.state.product.product.selected_images.front.display.fr
                            }}
                        />
                        <View style={styles.viewAbsolute}>
                            { this.state.product.product.nutriscore_grade != null 
                                ? this.nutriScore(this.state.product)
                                : null
                            } 
                        </View>

                        <Text style={styles.title}>{this.state.product.product.product_name}</Text>
                        <Text style={styles.subtitle}>{this.state.product.product.brands_tags[0].charAt(0).toUpperCase() + this.state.product.product.brands_tags[0].slice(1)}</Text>
                        { this.state.product.scanDate != null
                            ? <Text style={styles.date}>Scanné le { this.state.product.scanDate } </Text>
                            : null
                        }
                        <Text style={{marginTop:24, fontSize: 20, fontWeight: "bold", marginHorizontal:16}}>Informations</Text>
                        <View style={{marginHorizontal: 16}}>
                            { this.state.product.product.additives_n != 0 ? this._info("Additifs", this.state.product.product.additives_n, "") : null } 
                            { this.state.product.product.nutriments.energy != null ? this._info("Calories", this.state.product.product.nutriments.energy, "kCal") : null }
                            { this.state.product.product.nutriments.sugars != null ? this._info("Sucre", this.state.product.product.nutriments.sugars, "g") : null }
                            { this.state.product.product.nutriments.salt != null ? this._info("Sel", this.state.product.product.nutriments.salt, "g") : null }
                            { this.state.product.product.nutriments.fat != null ? this._info("Gras", this.state.product.product.nutriments.fat, "g") : null }
                            { this.state.product.product.nutriments.proteins ? this._info("Proteines", this.state.product.product.nutriments.proteins, "g") : null }
                            { this.state.product.product.nutriments.fiber != null ? this._info("Fibres", this.state.product.product.nutriments.fiber, "g") : null }
                        </View>
                    </ScrollView>

                    { this.state.new 
                        ?   <TouchableOpacity style={{borderColor: "black", borderWidth: 1, borderRadius: 20, height:40, left:16, right:16, backgroundColor:"white", position: "absolute", bottom:16, zIndex:10}} onPress={this._goToScan}>
                                <Text style={{ fontSize: 20, color: 'black', flex: 1, alignSelf:"center", paddingTop:5}}>Nouveau scan</Text>
                            </TouchableOpacity>
                        : null
                    }
                </SafeAreaView>
            );
        }
    }
}

const styles = StyleSheet.create({
    imageHeader: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width
    },
    info: {
        marginTop: 4,
        marginLeft: 16,
        fontSize: 14, 
        fontWeight:"bold",
        color:"gray"
    },
    titleInfo: {
        marginRight: 16,
        fontSize: 18, 
    },
    title: {
        marginTop: 16,
        marginHorizontal: 16,
        fontSize: 24,
        fontWeight: "bold"
    },
    subtitle: {
        marginHorizontal: 16,
        fontSize: 20,
        color: "gray"
    },
    date: {
        marginHorizontal: 16,
        marginTop: 10,
        fontSize: 18,
        color: "gray",
        opacity: 0.5
    },
    viewAbsolute: {
        width: 50,
        height: 50,
        borderRadius: 50,
        position: "absolute",
        top: Dimensions.get('window').width * 0.8,
        right: 16
    }

});
