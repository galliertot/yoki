import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default class Scan extends React.Component {

  constructor(props) {
    super(props);
    this.state = { 
      hasPermission: null, 
      scanned: false, 
      setScanned: false };
  }

  componentDidMount() {
    this.request()
  }

  request = async () => {
    const { status } = await BarCodeScanner.requestPermissionsAsync();
    this.setState({
      hasPermission : (status === 'granted')
    })
  } 

  handleBarCodeScanned = async ({ type, data }) => {
    this.state.setScanned = true
    this.props.navigation.navigate('Produit', { code : data, new: true, date: null} )
  };
  
  render() {
    if (this.state.hasPermission) {
      return (
        <View style={styles.container}>
          <BarCodeScanner
            onBarCodeScanned={this.state.scanned ? undefined : this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />
        </View>
      );
    }

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
            <View style={{ flex: 1, justifyContent:"center", alignItems: "center", verticalAlign: "middle",}}>
                <Text style={{fontSize:24, fontWeight: "bold", textAlign: "center", marginHorizontal: 16}}>Autorisé l'accès à la caméra pour commencer à scanner</Text>
            </View>
        </SafeAreaView>
    )
    
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});


