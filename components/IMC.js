import React from 'react';
import { StyleSheet, Text, SafeAreaView, View, TouchableOpacity, Slider, Dimensions } from 'react-native';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            sexe: "male",
            height: 170,
            weight: 70,
            imc: null
        };
    }

    componentDidMount() { }

    _clicSexe = (item) => {
        this.setState({
            sexe: item
        })
    }

    _heightValueChanged = (value) => {
        this.setState({
            height: value
        })
    }

    _weightValueChanged = (value) => {
        this.setState({
            weight: value
        })
    }

    _calculIMC = () => {
        this.setState({
            IMC: ((this.state.weight / (this.state.height * this.state.height)) * 10000).toFixed(1)
        })
    }

    _category = () => {
        if (this.state.IMC < 16.5) { return "Dénutrition ou anorexie" }
        if (this.state.IMC > 16.5 && this.state.IMC < 18.5) { return "Maigreur" }
        if (this.state.IMC > 18.5 && this.state.IMC < 25) { return "Poids normal" }
        if (this.state.IMC > 25 && this.state.IMC < 30) { return "Surpoids" }
        if (this.state.IMC > 30 && this.state.IMC < 35) { return "Obésité modérée" }
        if (this.state.IMC > 35 && this.state.IMC < 40) { return "Obésité modérée" }
        if (this.state.IMC > 40) { return "Obésité morbide ou massive" }
    }

    render() {
        let styleButtonMale  = this.state.sexe === "male" ? [styles.sexeButton, {backgroundColor: "#1EB1FC"}] : [styles.sexeButton]
        let styleTitleButtonMale = this.state.sexe === "male" ? [styles.titleSexeButton, {color: "white"}] : [styles.titleSexeButton]

        let styleButtonFemale = this.state.sexe === "female" ? [styles.sexeButton, {backgroundColor: "#1EB1FC"}] : [styles.sexeButton]
        let styleTitleButtonFemale = this.state.sexe === "female" ? [styles.titleSexeButton, {color: "white"}] : [styles.titleSexeButton]

        let valueCorrect = (this.state.weight > 9 && this.state.height) > 99 ? true : false

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                <Text style={{marginTop: 32, marginLeft: 16, fontSize:22}}>IMC Calculator</Text>
                <View style={styles.container}>
                    <Text style={styles.subtitle}>Sexe</Text>
                    <View style={{ flexDirection:"row"}}>
                        <TouchableOpacity style={styleButtonMale} onPress={() => this._clicSexe("male")}>
                            <Text style={styleTitleButtonMale}>Homme</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styleButtonFemale} onPress={() => this._clicSexe("female")}>
                            <Text style={styleTitleButtonFemale}>Femme</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.subtitle}>Taille</Text>

                    <Text style={{marginLeft: this.state.height + 16 }}>{this.state.height} cm</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={300}
                        minimumTrackTintColor="#1EB1FC"
                        maximumTractTintColor="#1EB1FC"
                        step={1}
                        value={this.state.height}
                        onValueChange={value => this._heightValueChanged(value)}
                        style={styles.slider}
                        thumbTintColor="#1EB1FC"
                    />

                    <Text style={styles.subtitle}>Poids</Text>

                    <Text style={{marginLeft: this.state.weight / 2 + 16 }}>{this.state.weight} kg</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={600}
                        minimumTrackTintColor="#1EB1FC"
                        maximumTractTintColor="#1EB1FC"
                        step={1}
                        value={this.state.weight}
                        onValueChange={value => this._weightValueChanged(value)}
                        style={styles.slider}
                        thumbTintColor="#1EB1FC"
                    />

                    { this.state.IMC != null 
                        ?   <View>
                                <Text style={[styles.subtitle, {textAlign:"center", marginTop: 32, fontSize: 50, color: "black"}]}>{ this.state.IMC }</Text>
                                <Text style={[styles.subtitle, {textAlign:"center", marginTop: 0, color:'black'}]}>VOTRE IMC</Text>
                                <Text style={[styles.subtitle, {textAlign:"center", marginTop: 0, color:'black'}]}>{ this._category() }</Text>
                            </View>
                        :   null
                    }

                    
                    {   valueCorrect && (
                            <TouchableOpacity style={[styles.buttonDisabled]} onPress={this._calculIMC}>
                                <Text style={{ fontSize: 20, color: 'white', flex: 1, alignSelf:"center", paddingTop:5}}>Calculer</Text>
                            </TouchableOpacity>
                        ) || !valueCorrect && (
                            <TouchableOpacity disabled style={[styles.buttonDisabled, { borderColor: "gray",backgroundColor:"gray"}]}>
                                <Text style={{ fontSize: 20, color: 'white', flex: 1, alignSelf:"center", paddingTop:5}}>Calculer</Text>
                            </TouchableOpacity>
                        )
                    }
                    
                </View>
            </SafeAreaView>
        )

    }
}

const styles = StyleSheet.create({

    slider: {
        width: Dimensions.get('window').width - 32,
        marginHorizontal: 16
    },


    container: {
        flex: 1
    }, 

    subtitle: {
        fontSize:22,
        fontWeight: "bold",
        marginHorizontal: 16,
        marginVertical:16,
    },  

    sexeButton: {
        borderColor: "#1EB1FC", 
        borderWidth: 1, 
        borderRadius: 20, 
        height:40, 
        flex: 1,
        marginHorizontal: 16
    },

    titleSexeButton: {
        fontSize: 20, 
        flex: 1, 
        alignSelf:"center", 
        paddingTop:5
    },

    buttonDisabled : {
        borderWidth: 1, 
        borderRadius: 20, 
        height:40, 
        backgroundColor:"#1EB1FC",
        borderColor:"#1EB1FC",
        left:16, 
        right:16, 
        position: "absolute", 
        bottom:16, 
        zIndex:10 
    }
});
