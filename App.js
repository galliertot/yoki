import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './components/Home'
import Scan from './components/Scan'
import Product from './components/Product'
import IMC from './components/IMC'

const TabBarController = createBottomTabNavigator();

const ScanStack = createStackNavigator();
const HomeStack = createStackNavigator();


function settingStackScan() {
  return (
    <ScanStack.Navigator screenOptions={{headerShown: false}}>
      <ScanStack.Screen name="Scanner" component={Scan} />
      <ScanStack.Screen name="Produit" component={Product} />
    </ScanStack.Navigator>
  );
}


function settingStackHome() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Accueil" component={Home} options={{headerShown: false}}/>
      <ScanStack.Screen name="Produit" component={Product} options={{title: ''}} />
    </HomeStack.Navigator>
  );
}


export default function App() {
  return (
    <NavigationContainer>
      <TabBarController.Navigator
        tabBarOptions={{
          activeTintColor: 'black',
        }}
      >
        <TabBarController.Screen 
          name="Accueil" 
          component={settingStackHome}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }} 
        />
        <TabBarController.Screen 
          name="Scanner" 
          component={settingStackScan}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="camera" color={color} size={size} />
            ),
          }} 
        />
        <TabBarController.Screen 
          name="IMC" 
          component={IMC}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="account" color={color} size={size} />
            ),
          }} 
        />
      </TabBarController.Navigator>
    </NavigationContainer>
  )
}